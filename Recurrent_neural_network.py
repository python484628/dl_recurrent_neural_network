
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# May 2023


############# Example of Recurrent Neural Network RNN


########### Load libraries
from __future__ import print_function
from keras.callbacks import LambdaCallback
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from tensorflow.keras.optimizers import RMSprop
from keras.utils.data_utils import get_file

import numpy as np
import random
import sys
import io



# Import a text file (written by Nietzsche)
path = get_file('nietzsche.txt', origin='https://s3.amazonaws.com/text-datasets/nietzsche.txt')

with io.open(path, encoding='utf-8') as f:
    text = f.read().lower()

# Replace \n by blank spaces and print length of the corpus
text = text.replace("\n", " ")
print('corpus length: ', len(text))

# Define chars variable with its indices
chars = sorted(list(set(text)))
print('total chars: ', len(chars))
char_indices = dict((c,i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))



# Define maxlen, step variables and empty lists
maxlen = 40 # maximum size for the sequences (40 characters)
step = 3
sentences = []
next_chars = []

# For loop to fill the lists
for i in range(0, len(text) - maxlen, step):
    sentences.append(text[i: i+ maxlen])
    next_chars.append(text[i + maxlen])

#  How many sequences do we have    
print('nb sequences: ', len(sentences))

# Define x and y variables
print('Vectorization...')
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        x[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1
    
# i index of the sentence
# t index of the word in the sentence
# x which character for each word of each sentence

    
    
# Build model
print('Create the model...')
model = Sequential()
model.add(LSTM(128, input_shape=(maxlen, len(chars))))
model.add(Dense(len(chars), activation='softmax'))


# Define optimizer and create model with model.compile()
optimizer = RMSprop(learning_rate=0.01)
model.compile(loss ='categorical_crossentropy', optimizer=optimizer)


# 2 functions to train the model and see the text over iterations
def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


def on_epoch_end(epoch, _): # variable anonyme le tiret
    print()
    print('----- Generating text after Epoch: %d' % epoch)
    
    start_index = random.randint(0, len(text) - maxlen -1)
    for diversity in [0.2, 0.5, 1.0, 1.2]:
        print('------ diversity: ', diversity)
        
        generated = ''
        sentence = text[start_index: start_index + maxlen]
        generated += sentence
        print('------- generating with seed: "' + sentence + '"')
        sys.stdout.write(generated)
        
        for i in range(400):
            x_pred = np.zeros((1, maxlen, len(chars)))
            for t,char in enumerate(sentence):
                x_pred[0, t, char_indices[char]] = 1.
                
            preds = model.predict(x_pred, verbose=0)[0]
            next_indec = sample(preds, diversity)
            next_char = indices_char[next_indec]
            
            sentence = sentence[1:] + next_char
            
            sys.stdout.write(next_char)
            sys.stdout.flush()
            
        print()

# Define print_callback variable
print_callback = LambdaCallback(on_epoch_end=on_epoch_end)
# If we have a callback the function will be used      

# Fit the model (wee cann see the number of epochs and loss values over the iterations)
model.fit(x,y,
          batch_size=100,
          epochs=50,
          callbacks=[print_callback])













